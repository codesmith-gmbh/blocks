(ns build
  (:require [ch.codesmith.anvil.libs :as libs]
            [ch.codesmith.anvil.release :as rel]
            [ch.codesmith.anvil.shell :as sh]
            [clojure.java.io :as io]
            [clojure.tools.build.api :as b]
            [babashka.fs :as fs]))

(def lib-namespace "ch.codesmith")

(def libs (mapv
            #(symbol lib-namespace (fs/file-name %))
            (fs/list-dir
              (fs/path "libraries"))))

(def version (str "0.11." (b/git-count-revs {})))

(defn build-jar [library]
  (let [root-dir (io/file "libraries" (name library))]
    (libs/jar {:lib              library
               :polylibs         (set libs)
               :version          version
               :with-pom?        true
               :description-data {:scm     {:type         :gitlab
                                            :organization "codesmith-gmbh"
                                            :project      "blocks"}
                                  :license :epl}
               :root             root-dir})))

(defn build-jars [_]
  (doseq [library libs]
    (build-jar library)))

(defn deploy-jar [library]
  (let [root-dir (io/file "libraries" (name library))
        jar-file (build-jar library)]
    (libs/deploy {:jar-file       jar-file
                  :lib            library
                  :root-dir       root-dir
                  :sign-releases? false})))

(defn deploy-jars []
  (doseq [library libs]
    (deploy-jar library)))

(defn release [_]
  (deploy-jars)
  (rel/git-release! {:artifacts           (mapv
                                            (fn [lib]
                                              {:deps-coords   lib
                                               :artifact-type :mvn})
                                            libs)
                     :version             version
                     :release-branch-name "main"}))
