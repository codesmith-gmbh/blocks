(ns stan
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.dev.agent :as cdba]
            [clojure.walk :as walk]
            [integrant.core :as ig])
  (:import (ch.codesmith.blocks SymbolicRef)))

(def special-values [:a :b :c])

(def system
  {::config {[:dev 'special-value]  {:type   ::type-2
                                     :config {:name "stan"}}
             [:prod 'special-value] {:type   ::type-1
                                     :config {:name "oli"
                                              :dev  (cb/symbolic-ref [::type :dev 'special-value])}}}})

(defmethod ig/expand-key ::config [_ envs]
  (into {}
    (mapcat (fn [[[env special-value-symb] {:keys [type config]}]]
              (mapv
                (fn [special-value]
                  [[::type env special-value]
                   {:type type
                    :config
                    (walk/postwalk
                      (fn [form]
                        (cond
                          (= special-value-symb form)
                          special-value

                          (instance? SymbolicRef form)
                          (cb/ref (mapv
                                    (fn [k]
                                      (if (= special-value-symb form) special-value k))
                                    (:ref form)))

                          :else form))
                      (assoc config :special-value special-value))}])
                special-values)))
    envs))

(defmethod ig/init-key ::type-1 [_ {:keys [name dev special-value]}]
  ["type1" name dev special-value])

(defmethod ig/init-key ::type-2 [_ {:keys [name dev special-value]}]
  [name "type2" dev special-value])

(derive ::type-1 ::type)
(derive ::type-2 ::type)

(def base
  {:name   :dev
   :blocks (ig/expand system)})

(comment

  (cdba/init! [base])
  (cdba/ignite! [::type :prod :a])

  )
