# Codesmith Blocks

[![Clojars Project](https://img.shields.io/clojars/v/ch.codesmith/blocks.svg)](https://clojars.org/ch.codesmith/blocks)

Building blocks using James Reeves Integrant.

To use, include in your deps.edn the libraries that you need.

```deps
ch.codesmith/blocks {:mvn/version "0.11.156"}
ch.codesmith/blocks-ring {:mvn/version "0.11.156"}
ch.codesmith/blocks-rabbitmq-dev {:mvn/version "0.11.156"}
ch.codesmith/blocks-dev {:mvn/version "0.11.156"}
ch.codesmith/blocks-xtdb {:mvn/version "0.11.156"}
ch.codesmith/blocks-jdbc {:mvn/version "0.11.156"}
ch.codesmith/blocks-sqlite {:mvn/version "0.11.156"}
ch.codesmith/blocks-postgres {:mvn/version "0.11.156"}
ch.codesmith/blocks-rabbitmq {:mvn/version "0.11.156"}
ch.codesmith/blocks-postgres-dev {:mvn/version "0.11.156"}
ch.codesmith/blocks-reitit {:mvn/version "0.11.156"}
ch.codesmith/blocks-aero {:mvn/version "0.11.156"}
ch.codesmith/blocks-test-containers {:mvn/version "0.11.156"}
ch.codesmith/blocks-reitit-dev {:mvn/version "0.11.156"}
ch.codesmith/blocks-httpkit {:mvn/version "0.11.156"}
ch.codesmith/blocks-jdbc-dev {:mvn/version "0.11.156"}
```

## License

Copyright © 2021-2022 Codesmith GmbH

Released under Eclipe Public License v1.0, same as Clojure