(ns ch.codesmith.blocks.postgres-test
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.jdbc :as cbjdbc]
            [ch.codesmith.blocks.jdbc.liquibase :as liquibase]
            [ch.codesmith.blocks.postgres :as cbp]
            [clojure.test :refer [deftest is]]
            [next.jdbc :as jdbc]
            [prestancedesign.get-port :refer [get-port]])
  (:import (com.zaxxer.hikari HikariDataSource)
           (io.zonky.test.db.postgres.embedded EmbeddedPostgres)))

(deftest postgres-correctness
  (with-open [embedded-postgres (.start (.setPort (EmbeddedPostgres/builder) (get-port)))]
    (let [version "1.0.0"
          base    {:name   :postgres
                   :blocks {::postgres {:type   cbp/postgres-datasource
                                        :config {:migration-function liquibase/liquibase-migrate-function
                                                 :migration-config   {:change-log-file  "postgres/migrations/changelog.yaml"
                                                                      :version          version
                                                                      :liquibase-schema "public"
                                                                      :default-schema   "public"}}}}}
          profile {:profile :test
                   :blocks  {::postgres {:type   ::cbp/external-datasource
                                         :config {:datasource {:jdbcUrl  (.getJdbcUrl embedded-postgres "postgres" "postgres")
                                                               :password "postgres"}}}}}]
      (cb/with-system
        [system [base profile]]
        (let [^HikariDataSource ds (cb/resolve-block system ::postgres)]
          (is (map? (cbjdbc/data-source-config ds)))
          (is (= 1 (:test/test (jdbc/execute-one! ds ["SELECT TEST FROM TEST"]))))
          (let [changelogs (jdbc/execute! ds ["SELECT * from DATABASECHANGELOG"])]
            (is (= 1 (count changelogs)))
            (let [{:databasechangelog/keys [id tag]} (first changelogs)]
              (is (= version tag))
              (is (= "20220428000000-create-test-view" id)))))))))
