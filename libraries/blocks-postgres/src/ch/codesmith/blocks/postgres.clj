(ns ch.codesmith.blocks.postgres
  (:require [ch.codesmith.blocks.jdbc :as cbjdbc]
            [integrant.core :as ig]))

(def postgres-datasource ::datasource)
(derive postgres-datasource cbjdbc/jdbc-datasource)

(def config-defaults
  {:reWriteBatchedInserts true})

(defmethod ig/init-key ::external-datasource [_ config]
  (cbjdbc/init (merge config-defaults config)))

(defmethod ig/halt-key! ::external-datasource [_ ds]
  (cbjdbc/halt! ds))

(derive ::external-datasource postgres-datasource)
