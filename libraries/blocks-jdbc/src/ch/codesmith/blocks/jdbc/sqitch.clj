(ns ch.codesmith.blocks.jdbc.sqitch
  (:require [next.jdbc :as jdbc]))

(defn tag-check-query [sqitch-schema experted-tag]
  [(str "select * from " sqitch-schema ".tags where tag = ?") experted-tag])

(defn sqitch-migration-check [ds {:keys [sqitch-schema expected-tag]
                                  :or   {sqitch-schema "sqitch"}}]
  (let [tag (jdbc/execute-one!
              ds
              (tag-check-query sqitch-schema expected-tag))]
    (when-not tag
      (throw (ex-info (str "The tag " expected-tag " has not been installed by sqitch")
                      {:tag expected-tag})))
    tag))
