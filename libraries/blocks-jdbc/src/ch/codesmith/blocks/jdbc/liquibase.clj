(ns ch.codesmith.blocks.jdbc.liquibase
  (:require [next.jdbc :as jdbc])
  (:import (liquibase Liquibase)
           (liquibase.database DatabaseFactory)
           (liquibase.database.jvm JdbcConnection)
           (liquibase.resource ClassLoaderResourceAccessor)))

(defn liquibase-migrate-function [ds {:keys [^String change-log-file ^String version ^String liquibase-schema ^String default-schema]}]
  (jdbc/on-connection [conn ds]
    (let [db-conn   (JdbcConnection. conn)
          database  (.findCorrectDatabaseImplementation (DatabaseFactory/getInstance) db-conn)]
      (when liquibase-schema
        (.setLiquibaseSchemaName database liquibase-schema))
      (when default-schema
        (.setDefaultSchemaName database default-schema))
      (let [liquibase (Liquibase.
                        change-log-file
                        (ClassLoaderResourceAccessor.)
                        database)]
        (.update liquibase)
        (when version
          (.tag liquibase version))))))
