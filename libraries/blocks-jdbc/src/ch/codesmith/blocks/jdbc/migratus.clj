(ns ch.codesmith.blocks.jdbc.migratus
  (:require [migratus.core :as migratus])
  (:import (java.sql Connection)
           (javax.sql DataSource)))

(defmulti migratus-db-map class)

(defmethod migratus-db-map
  :default [value] value)

(defmethod migratus-db-map
  Connection [conn] {:connection conn})

(defmethod migratus-db-map
  DataSource [ds] {:datasource ds})

(defn migratus-migrate-function [connectable migratus-config]
  (let [config (merge {:db    (migratus-db-map connectable)
                       :store :database}
                      migratus-config)]
    (migratus/init config)
    (migratus/migrate config)))