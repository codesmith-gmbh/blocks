(ns ch.codesmith.blocks.jdbc
  (:require
    [ch.codesmith.blocks :as cb]
    [clojure.spec.alpha :as s]
    [next.jdbc :as jdbc]
    [next.jdbc.connection :as conn])
  (:import (com.zaxxer.hikari HikariDataSource)))

;; # Block Hierarchy root

(def jdbc-datasource ::datasource)

;; # Migrations

(defn migrate-db! [ds {:keys [migration-config migration-function]}]
  (when migration-function
    (migration-function ds migration-config)))

;; # configuration

(s/def ::jdbcUrl string?)

(s/def ::datasource (s/keys :req-un [::jdbcUrl]))
(s/def ::migration-function (fn [x]
                              (or (fn? x)
                                  (var? x))))
(s/def ::migration-config any?)

(s/def ::configuration (s/keys :req-un [::datasource]
                               :opt-un [::migration-config ::migration-function]))

(def configuration-checker (cb/checker ::configuration))

(def datasource-defaults
  {:datasource {:autoCommit false}})

;; # next.jdbc options

(defn ensure-options [ds {:keys [next-jdbc-options]}]
  (if next-jdbc-options
    (jdbc/with-options ds next-jdbc-options)
    ds))

(defn unwrap ^HikariDataSource [ds]
  (if (record? ds)
    (:connectable ds)
    ds))

;; # blocks helper

(defn init [config]
  (let [config (configuration-checker (cb/deep-merge
                                        datasource-defaults
                                        config))
        ds     (conn/->pool HikariDataSource (:datasource config))]
    (migrate-db! ds config)
    (ensure-options ds config)))

(defn halt! [ds]
  (.close (unwrap ds)))

(defn data-source-config [^HikariDataSource ds]
  {:jdbcUrl              (.getJdbcUrl ds)
   :username             (.getUsername ds)
   :readOnly             (.isReadOnly ds)
   :driverClassName      (.getDriverClassName ds)
   :dataSourceProperties (into {} (.getDataSourceProperties ds))})
