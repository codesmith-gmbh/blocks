(ns ch.codesmith.blocks.jdbc.dev
  (:require [ch.codesmith.blocks.dev :as cbd]
            [next.jdbc :as jdbc]))

(defn get-ds [block-key]
  (cbd/resolve-block block-key))

(defn get-connection [block-key]
  (jdbc/get-connection (get-ds block-key)))

(defmacro with-transaction [[sym block-key] & body]
  `(with-open [conn# (get-connection ~block-key)]
     (jdbc/with-transaction [~sym conn#]
                            ~@body)))