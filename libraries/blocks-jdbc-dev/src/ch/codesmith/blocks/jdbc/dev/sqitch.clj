(ns ch.codesmith.blocks.jdbc.dev.sqitch
  (:require [babashka.process :as ps]
            [ch.codesmith.blocks.dev :as cbd]
            [integrant.core :as ig]))

(defn sqitch-options [options]
  (mapcat (fn [[keys value]]
            (let [arg-key (str "--" (name keys))]
              (if (coll? value)
                (mapcat (fn [value]
                          [arg-key (str value)])
                        value)
                [arg-key (str value)])))
          options))

(defn exec-sqitch [exec command options]
  (apply ps/shell exec (name command) (sqitch-options options))
  nil)

(defmethod ig/init-key ::sqitch
  [_ {:keys [sqitch-command migration-directory]
      :or   {sqitch-command      "sqitch"
             migration-directory "."}}]
  (fn sqitch
    ([command]
     (sqitch command {}))
    ([command options]
     (exec-sqitch sqitch-command command (merge {:chdir migration-directory}
                                                options)))))

;; WARNING: the 2 following functions works only if you have a default target configured for local development (ONLY!)

(defn sqitch
  ([command]
   (sqitch command {}))
  ([command options]
   ((cbd/resolve-block ::sqitch) command options)))

(defn sqitch-migration [_ sqitch]
  (sqitch :deploy))

(defn assoc-sqitch-migration [jdbc-config]
  (assoc jdbc-config
    :migration-function sqitch-migration
    :migration-config (ig/ref ::sqitch)))
