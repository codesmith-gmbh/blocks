(ns ch.codesmith.blocks.jdbc.dev.sourceable-keywords
  (:require [ch.codesmith.blocks.dev :as cbd]
            [next.jdbc.protocols :as njp])
  (:import (clojure.lang Keyword)
           (javax.sql DataSource)))

(extend-protocol njp/Sourceable
  Keyword
  (get-datasource ^DataSource [this]
    (cbd/resolve-block this)))
