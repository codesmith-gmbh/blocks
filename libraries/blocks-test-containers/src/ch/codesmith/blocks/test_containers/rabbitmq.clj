(ns ch.codesmith.blocks.test-containers.rabbitmq
  (:require [clj-test-containers.core :as tc]
            [clojure.java.browse :as cjb]))

(def test-erlang-cookie "test-erlang-cookie")

(defn rabbitmq-container
  ([]
   (rabbitmq-container {}))
  ([{:keys [image-name exposed-ports
            erlang-cookie enabled-plugins-file]
     :or   {image-name           "rabbitmq:3.9"
            exposed-ports        [5672 5673 15672]
            erlang-cookie        test-erlang-cookie
            enabled-plugins-file {:path "ch/codesmith/blocks/test_containers/rabbitmq/enabled_plugins"
                                  :type :classpath-resource}}}]
   (-> (tc/create {:image-name    image-name
                   :exposed-ports exposed-ports
                   :env-vars      {"RABBITMQ_ERLANG_COOKIE" erlang-cookie
                                   "RABBITMQ_DEFAULT_VHOST" "/"}
                   :wait-for      {:wait-strategy   :log
                                   :message         "Server startup complete"
                                   :startup-timeout 30}})
       (tc/copy-file-to-container!
         (assoc enabled-plugins-file
           :container-path "/etc/rabbitmq/enabled_plugins")))))

(defn connection-config
  ([rabbitmq-container]
   (connection-config rabbitmq-container {}))
  ([rabbitmq-container {:keys [host username password]
                        :or   {host     "localhost"
                               username "guest"
                               password "guest"}}]
   {:port     ((:mapped-ports rabbitmq-container) 5672)
    :host     host
    :username username
    :password password}))

(defn open-admin-web-ui [rabbitmq-container]
  (cjb/browse-url (str "http://localhost:" (get (:mapped-ports rabbitmq-container) 15672))))

(defmacro with-rabbitmq
  [[var settings] & body]
  `(let [~var (-> (rabbitmq-container ~settings)
                  (tc/start!))]
     (try
       ~@body
       (finally
         (tc/stop! ~var)))))
