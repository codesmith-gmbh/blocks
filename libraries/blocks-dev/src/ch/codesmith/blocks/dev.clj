(ns ch.codesmith.blocks.dev
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.dev.state :as cbds]
            [integrant.core :as ig]))


(defn halt! []
  (alter-var-root #'cbds/system (fn [system]
                                  (when system
                                    (cb/halt! system))
                                  nil))
  (alter-var-root #'cbds/config (constantly nil)))

(defn- go!′ [base+profile init-fn]
  (halt!)
  (let [config (apply cb/merge-with-profiles base+profile)]
    (alter-var-root #'cbds/config (constantly config))
    (try
      (let [system (init-fn config)]
        (alter-var-root #'cbds/system (constantly system)))
      (catch Exception e
        (when-let [ig-system (:system (ex-data e))]
          (ig/halt! ig-system))
        (throw e))))
  :done)

(defn go!
  ([base+profile]
   (go!′ base+profile #'cb/init′))
  ([base+profile keys]
   (go!′ base+profile #(#'cb/init′ % keys))))

(defn get-block [block-key]
  (cb/get-block cbds/system block-key))

(defn resolve-block [block-key]
  (cb/resolve-block cbds/system block-key))
