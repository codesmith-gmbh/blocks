(ns ch.codesmith.blocks.dev.agent
  (:require [ch.codesmith.blocks :as cb]
            [clojure.tools.logging :as log])
  (:import (java.util.concurrent CompletableFuture TimeUnit)))

(defonce instance (agent nil :error-handler (fn [_ e]
                                              (log/error e))))

(defn init! [base+profiles]
  (send-off instance (constantly (cb/ignite! base+profiles))))

(defn- ensure-component! [key via-f]
  (let [result (CompletableFuture.)]
    (send-off instance (fn [state]
                         (let [new-state (via-f state [key])]
                           (.complete
                             result
                             (cb/resolve-block new-state key))
                           new-state)))
    (.get result 30 TimeUnit/SECONDS)))

(defn ignite! [key]
  (if-let [component (cb/resolve-block @instance key)]
    component
    (ensure-component! key cb/ignite!)))

(defn quench! [key]
  (send-off instance cb/quench! [key]))

(defn re-ignite! [key]
  (ensure-component! key cb/re-ignite!))

(defn go! [base+profiles]
  (cb/halt! @instance)
  (init! base+profiles))
