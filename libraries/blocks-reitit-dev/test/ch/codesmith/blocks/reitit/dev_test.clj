(ns ch.codesmith.blocks.reitit.dev-test
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.reitit :as cbrei]
            [ch.codesmith.blocks.reitit.dev :as cbreid]
            [clojure.test :refer [deftest is testing]]
            [reitit.core :as r])
  (:import (clojure.lang ExceptionInfo)))

(defn routes-fn []
  ["/hello" {:name :hello}])

(defn route-name-for-hello-path [router]
  (:name (:data (r/match-by-path router "/hello"))))

(defn test-routers [routes-fn force? test-fn]
  (let [router-config {:config {:routes-fn routes-fn}}
        base {:name   :dynamic-router
              :blocks {::dynamic-router router-config
                       ::static-router  router-config}}
        profile {:profile :test
                 :blocks  {::dynamic-router {:type   ::cbreid/dynamic-router
                                             :config {:force? force?}}
                           ::static-router  {:type ::cbrei/static-router}}}]
    (test-fn base profile)))

(deftest dynamic-router-correctness
  (let [test-fn (fn [base profile]
                  #_{:clj-kondo/ignore [:inline-def]}
                  (defn routes-fn []
                    ["/hello" {:name :hello}])
                  (cb/with-system [system [base profile]]
                    (let [static-router (cb/get-block system ::static-router)
                          dynamic-router (cb/get-block system ::dynamic-router)]
                      (is (= :hello (route-name-for-hello-path static-router)))
                      (is (= :hello (route-name-for-hello-path dynamic-router)))
                      #_{:clj-kondo/ignore [:inline-def]}
                      (defn routes-fn []
                        ["/hello" {:name :hello2}])
                      (is (= :hello (route-name-for-hello-path static-router)))
                      (is (= :hello2 (route-name-for-hello-path dynamic-router))))))]
    (testing "with simple var"
      (test-routers #'routes-fn false test-fn))
    (testing "with function value with var evaluation inside"
      (test-routers (fn [] (routes-fn)) true test-fn))))

(deftest dynamic-router-completeness
  (is (thrown? ExceptionInfo
               (test-routers routes-fn false
                             (fn [base profile]
                               (cb/init base profile))))))
