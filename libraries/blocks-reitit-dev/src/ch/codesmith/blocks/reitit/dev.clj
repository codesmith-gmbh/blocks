(ns ch.codesmith.blocks.reitit.dev
  (:require [ch.codesmith.blocks.reitit :as cbrei]
            [integrant.core :as ig]
            [reitit.core :as r]))

(deftype DynamicRouter [router-fn]
  r/Router
  (router-name [_]
    :dynamic-router)
  (routes [_]
    (r/routes (router-fn)))
  (compiled-routes [_]
    (r/compiled-routes (router-fn)))
  (options [_]
    (r/options (router-fn)))
  (route-names [_]
    (r/route-names (router-fn)))
  (match-by-path [_ path]
    (r/match-by-path (router-fn) path))
  (match-by-name
    [_ name]
    (r/match-by-name (router-fn) name))
  (match-by-name
    [_ name path-params]
    (r/match-by-name (router-fn) name path-params)))

(defmethod ig/init-key ::dynamic-router [_ {:keys [routes-fn
                                                      force?] :as config}]
  (when-not (or force? (var? routes-fn))
    (throw (let [class (class routes-fn)]
             (ex-info
               (str "To work properly, the dynamic router requires a var as routes-fn, got a " class
                    " add the :force key with a truthy value to override.")
               {:class class}))))
  (->DynamicRouter #(cbrei/static-router config)))

(derive ::dynamic-router ::cbrei/router)
