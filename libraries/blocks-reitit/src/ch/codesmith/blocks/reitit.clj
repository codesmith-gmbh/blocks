(ns ch.codesmith.blocks.reitit
  (:require [integrant.core :as ig]
            [muuntaja.core :as m]
            [reitit.coercion.malli :as rcm]
            [reitit.ring :as ring]
            [reitit.ring.coercion :as rrcoerce]
            [reitit.ring.middleware.exception :as rrme]
            [reitit.ring.middleware.muuntaja :as rrmm]
            [reitit.ring.middleware.parameters :as rrpa]))

(defn default-e-handler
  "Default safe handler for any exception."
  [^Exception e _]
  {:status 500
   :body   {:type    "exception"
            :message (.getMessage e)
            :data    (ex-data e)
            :class   (.getName (.getClass e))}})

(def default-middleware-stack
  [rrmm/format-negotiate-middleware
   rrmm/format-response-middleware
   (rrme/create-exception-middleware (assoc rrme/default-handlers
                                       ::rrme/default default-e-handler))
   rrmm/format-request-middleware
   rrpa/parameters-middleware
   rrcoerce/coerce-exceptions-middleware
   rrcoerce/coerce-request-middleware
   rrcoerce/coerce-response-middleware])

(defn static-router [{:keys [routes-fn middleware-stack]
                      :or   {middleware-stack default-middleware-stack}}]
  (ring/router (routes-fn)
               {:data {:coercion   rcm/coercion
                       :muuntaja   (m/create)
                       :middleware middleware-stack}}))

(defmethod ig/init-key ::static-router [_ config]
  (static-router config))

(derive ::static-router ::router)

(defmethod ig/init-key ::ring-handler [_ {:keys [router default-handler options]}]
  (ring/ring-handler router default-handler options))

(derive ::ring-handler :ch.codesmith.blocks.ring/handler)

(def ring-handler
  {:type   ::ring-handler
   :config {:router (ig/ref ::router)}})