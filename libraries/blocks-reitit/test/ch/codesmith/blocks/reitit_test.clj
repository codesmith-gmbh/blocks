(ns ch.codesmith.blocks.reitit-test
  (:require [clojure.test :refer [deftest is]]
            [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.reitit :as cbrei]
            [reitit.core :as r]))

(defn routes []
  ["/hello" {:name :hello}])

(deftest router-correctness
  (let [base {:name   :router
              :blocks {::router {:config {:routes-fn #'routes}}}}
        profile {:profile :test
                 :blocks  {::router {:type ::cbrei/static-router}}}]
    (cb/with-system [system [base profile]]
      (let [router (cb/resolve-block system ::router)]
        (is (= :hello
               (:name (:data (r/match-by-path router "/hello")))))))))
