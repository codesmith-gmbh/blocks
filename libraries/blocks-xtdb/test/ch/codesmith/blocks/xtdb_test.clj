(ns ch.codesmith.blocks.xtdb-test
  (:require [babashka.fs :as fs]
            [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.xtdb :as cbx]
            [clojure.test :refer [deftest is]]
            [xtdb.api :as xt]))

(defn init [node]
  (xt/submit-tx node [[::xt/put {:xt/id 1}]]))

(defn test-xtdb [xtdb-config]
  (let [base {:name   :xtdb
              :blocks {::xtdb {:type   ::cbx/node
                               :config xtdb-config}}}]
    (cb/with-system [system [base]]
      (let [node (cb/resolve-block system ::xtdb)
            db (xt/db node)]
        (is (= {:xt/id 1}
               (xt/entity db 1)))))))

(deftest xtdb-correctness
  (test-xtdb
    {:init init}))

(deftest xtdb-dev-rocksdb-correctness
  (let [tmpdir (fs/create-temp-dir)
        xtdir (fn [child] (str (fs/path tmpdir child)))]
    (test-xtdb (:config
                 (cbx/with-rocksdb
                   {:tx-log-path         (xtdir "tx-log")
                    :document-store-path (xtdir "doc-store")
                    :index-store-path    (xtdir "index-store")
                    :init                init})))))