(ns ch.codesmith.blocks.xtdb
  (:require [clojure.java.io :as io]
            [integrant.core :as ig]
            [xtdb.api :as xt])
  (:import (java.io Closeable)))

(defn stop-node [^Closeable xtdb-node]
  (.close xtdb-node))

(defmethod ig/init-key ::node [_ {:keys [node-options init]}]
  (let [node (xt/start-node (or node-options {}))]
    (when init
      (try
        (when-let [tx (init node)]
          (xt/await-tx node tx))
        (catch Exception e
          (stop-node node)
          (throw e))))
    node))

(defmethod ig/halt-key! ::node [_ node]
  (stop-node node))

(defn with-rocksdb [{:keys [tx-log-path document-store-path index-store-path init]
                    :or    {tx-log-path         "data/xtdb/tx-log"
                           document-store-path "data/xtdb/doc-store"
                           index-store-path    "data/xtdb/index-store"}}]
  (letfn [(kv-store [dir]
            {:kv-store {:xtdb/module 'xtdb.rocksdb/->kv-store
                        :db-dir      (io/file dir)
                        :sync?       true}})]
    {:type   ::node
     :config {:node-options {:xtdb/tx-log         (kv-store tx-log-path)
                             :xtdb/document-store (kv-store document-store-path)
                             :xtdb/index-store    (kv-store index-store-path)}
              :init         init}}))
