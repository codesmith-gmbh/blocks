(ns ch.codesmith.blocks.xtdb.ring
  (:require [xtdb.api :as xt]))

(defn inject-database [request xtdb-node]
  (update request :app/ctx #(assoc % :db (xt/db xtdb-node)
                                     :db-fn (fn [valid-time-or-basis]
                                              (xt/db xtdb-node valid-time-or-basis)))))

(defn perform-txs-if-necessary!
  [request {:keys [tx-ops resp-fn] :as response} additional-tx-fn xtdb-node]
  (cond
    (and tx-ops resp-fn) (let [tx-ops (additional-tx-fn tx-ops request)
                               tx     (xt/submit-tx xtdb-node tx-ops)]
                           (xt/await-tx xtdb-node tx)
                           (perform-txs-if-necessary!
                             request
                             (resp-fn (xt/db xtdb-node))
                             additional-tx-fn
                             xtdb-node))
    (or tx-ops resp-fn) (throw (ex-info "tx-ops and resp-fn must both be defined" {}))
    :else response))

(defn xtdb-middleware [handler {:keys [xtdb-node
                                       additional-tx-fn]}]
  (let [additional-tx-fn (or additional-tx-fn (fn [tx-ops _] tx-ops))]
    (fn
      ([request]
       (perform-txs-if-necessary!
         request
         (handler (inject-database request xtdb-node))
         additional-tx-fn
         xtdb-node))
      ([request respond raise]
       (handler (inject-database request xtdb-node)
         #(respond (perform-txs-if-necessary! request % additional-tx-fn xtdb-node)) raise)))))
