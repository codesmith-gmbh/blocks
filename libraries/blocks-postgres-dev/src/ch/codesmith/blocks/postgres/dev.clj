(ns ch.codesmith.blocks.postgres.dev
  (:require [babashka.process :as ps]
            [ch.codesmith.blocks.jdbc :as cbjdbc]
            [ch.codesmith.blocks.postgres :as cbp]
            [clojure.string :as str]
            [integrant.core :as ig]
            [next.jdbc :as jdbc])
  (:import (io.zonky.test.db.postgres.embedded EmbeddedPostgres)))

(defmulti stop-embedded-postgres
  {:arglists '([port])}
  (fn [_]
    (System/getProperty "os.name")))

(defn shell-str [command]
  (str/trim (:out (ps/shell {:out :string} command))))

(defn macos-pids-on-port [port]
  (map #(Integer/parseInt %) (str/split-lines (shell-str (str "lsof -ti tcp:" port)))))

(defn macos-ppid [pid]
  (Integer/parseInt (shell-str (str "ps -o ppid= " pid))))

(defn macos-command [pid]
  (shell-str (str "ps -o command= " pid)))

(defn macos-stop-command [^String postgres-command]
  (second (re-matches
            #"(.*pg_ctl stop -D \S*).*"
            (.replace postgres-command
              "/bin/postgres"
              "/bin/pg_ctl stop"))))

(defmethod stop-embedded-postgres "Mac OS X"
  [port]
  (try (ps/shell (->> port
                   macos-pids-on-port
                   (some #(and (= 1 (macos-ppid %)) %))
                   macos-command
                   macos-stop-command))
       (catch Exception _))
  nil)

(defmethod ig/init-key ::embedded [_ {:keys [force-stop? port init-sql-statements] :as config}]
  (when force-stop?
    (stop-embedded-postgres port))
  (let [builder                             (EmbeddedPostgres/builder)
        builder                             (if port (.setPort builder port) builder)
        ^EmbeddedPostgres embedded-postgres (.start builder)
        jdbc-url                            (.getJdbcUrl embedded-postgres "postgres" "postgres")
        config                              (update config :datasource
                                              #(assoc %
                                                 :jdbcUrl jdbc-url
                                                 :password "postgres"))]
    (when (seq init-sql-statements)
      (with-open [conn (jdbc/get-connection (:datasource config))]
        (doseq [sql init-sql-statements]
          (jdbc/execute! conn sql))))
    (let [ds (cbjdbc/init config)]
      {:datasource        ds
       :embedded-postgres embedded-postgres})))

(defmethod ig/resolve-key ::embedded [_ instance]
  (:datasource instance))

(defmethod ig/halt-key! ::embedded [_ {:keys [datasource ^EmbeddedPostgres embedded-postgres]}]
  (.close (cbjdbc/unwrap datasource))
  (.close embedded-postgres))

(derive ::embedded cbp/postgres-datasource)

;(defmulti squitch-url
;          (fn [jdbc-url]
;            (second
;              (re-matches #"jdbc:(\w*):.*" jdbc-url))))
;
;(defmethod squitch-url "postgresql" [jdbc-url])
;
;(comment
;
;
;  (squitch-url "jdbc:postgresql://localhost:11432/postgres")
;
;  )
;
;(defn sqitch-preparer ^DatabasePreparer [{:keys [sqitch-command migration-directory]
;                                          :or   {sqitch-command      "sqitch"
;                                                 migration-directory "."}}]
;  (reify DatabasePreparer
;    (prepare [this ds]
;      (let [ds (.unwrap ^DataSource ds BaseDataSource)]))))
