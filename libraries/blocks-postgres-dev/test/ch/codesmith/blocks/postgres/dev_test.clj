(ns ch.codesmith.blocks.postgres.dev-test
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.jdbc.migratus :as migratus]
            [ch.codesmith.blocks.postgres.dev :as cbpd]
            [clojure.test :refer [deftest is]]
            [next.jdbc :as jdbc]
            [next.jdbc.result-set :as rs]))

(def base {:name   :postgres-dev
           :blocks {::postgres {:type   ::cbpd/embedded
                                :config {:init-sql-statements [["create schema predefined"]]
                                         :migration-function  migratus/migratus-migrate-function
                                         :migration-config    {:migration-dir "postgres-dev/migrations"}}}}})

(defn execute-query [system]
  (let [ds (cb/resolve-block system ::postgres)]
    (jdbc/execute-one! ds ["select test from predefined.test"])))

(defn keys-from-query [system]
  (set (keys (execute-query system))))

(deftest postgres-dev-correctness
  (cb/with-system [system [base]]
    (is (= 2 (:test/test (execute-query system))))))

(deftest next-jdbc-with-options-correctness
  (let [with-options {:profile :with-options
                      :blocks  {::postgres {:config {:next-jdbc-options {:builder-fn rs/as-unqualified-maps}}}}}]
    (cb/with-system [system [base]]
      (is (= #{:test/test} (keys-from-query system))))
    (cb/with-system [system [base with-options]]
      (is (= #{:test} (keys-from-query system))))))

