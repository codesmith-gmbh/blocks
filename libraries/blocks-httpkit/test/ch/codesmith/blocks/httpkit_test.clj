(ns ch.codesmith.blocks.httpkit-test
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.httpkit :as cbh]
            [ch.codesmith.blocks.reitit :as cbrei]
            [clojure.test :refer [deftest is]]
            [hato.client :as hc]
            [prestancedesign.get-port :refer [get-port]]))

(defn routes []
  ["/hello" {:get (constantly {:status 200
                               :body   "hello"})}])

(deftest httpkit-correctness
  (let [port (get-port)
        base {:name   :httpkit
              :blocks {::router       {:type   ::cbrei/static-router
                                       :config {:routes-fn routes}}
                       ::ring-handler cbrei/ring-handler
                       ::server       cbh/server}}
        profile {:profile :test
                 :blocks  {::server {:config {:options {:port port}}}}}]
    (cb/with-system
      [_ [base profile]]
      (is (= "hello"
             (:body
               (hc/get (str "http://localhost:" port "/hello"))))))))