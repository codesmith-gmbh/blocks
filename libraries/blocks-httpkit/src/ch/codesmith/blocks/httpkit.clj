(ns ch.codesmith.blocks.httpkit
  (:require [integrant.core :as ig]
            [org.httpkit.server :as hkit]))

(defmethod ig/init-key ::server [_ {:keys [ring-handler
                                           options]}]
  (hkit/run-server ring-handler
                   (merge {:legacy-return-value? false}
                          options)))

(defmethod ig/halt-key! ::server [_ instance]
  (hkit/server-stop! instance))

(def server
  {:type   ::server
   :config {:ring-handler (ig/ref :ch.codesmith.blocks.ring/handler)}})
