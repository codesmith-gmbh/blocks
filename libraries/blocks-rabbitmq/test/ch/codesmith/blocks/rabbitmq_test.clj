(ns ch.codesmith.blocks.rabbitmq-test
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.rabbitmq :as cbr]
            [ch.codesmith.blocks.test-containers.rabbitmq :as tcr]
            [clojure.test :refer [deftest is testing]]
            [langohr.basic :as lb]
            [langohr.channel :as lch]
            [langohr.consumers :as lc]
            [langohr.queue :as lq])
  (:import (java.nio.charset StandardCharsets)))

(deftest rabbitmq-correctness
  (testing "Testing the correctness of the RabbitMQ block.
test largely inspired by the Hello World example of the langohr library."
    (tcr/with-rabbitmq [rmq {}]
      (let [base {:name   :rabbitmq
                  :blocks {::rabbitmq {:type   ::cbr/external-connection
                                       :config (tcr/connection-config rmq)}}}]
        (cb/with-system [instance [base]]
          (let [response (promise)
                connection (cb/resolve-block instance ::rabbitmq)
                qname "ch.codesmith.blocks.rabbitmq.test"
                message "test"]
            (with-open [ch (lch/open connection)]
              (lq/declare ch qname {:exclusive false :auto-delete true})
              (lc/subscribe ch qname (fn [_ _ ^bytes payload]
                                       (deliver response (String. payload StandardCharsets/UTF_8))))
              (lb/publish ch "" qname message {:content/type "text/plain"})
              (is (= message
                     (deref response 2000 nil))))))))))