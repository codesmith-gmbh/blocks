(ns ch.codesmith.blocks.rabbitmq
  (:require [integrant.core :as ig]
            [langohr.channel :as lc]
            [langohr.core :as rmq]))


(def connection ::connection)

(defmethod ig/init-key ::external-connection [_ config]
  (rmq/connect config))

(defmethod ig/halt-key! ::external-connection [_ instance]
  (rmq/close instance))

(derive ::external-connection connection)

(defmethod ig/init-key ::channel [_ {:keys [connection]}]
  (lc/open connection))

(defmethod ig/halt-key! ::channel [_ channel]
  (lc/close channel))

(def channel {:type   ::channel
              :config {:configuration (ig/ref connection)}})
