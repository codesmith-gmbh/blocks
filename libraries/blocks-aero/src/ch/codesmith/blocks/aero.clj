(ns ch.codesmith.blocks.aero
  (:require [aero.core :as aero]
            [clojure.java.io :as io]
            [integrant.core :as ig]))

(defn read-aero-config [{:keys [file resource profile]}]
  (aero/read-config (if-let [file (io/file file)]
                      (if (.exists file)
                        file
                        (throw (ex-info (str "file " file " does not exists") {:file file})))
                      (io/resource (or resource "config.edn")))
                    {:profile profile}))

(defmethod ig/init-key ::aero [_ config]
  (read-aero-config config))
