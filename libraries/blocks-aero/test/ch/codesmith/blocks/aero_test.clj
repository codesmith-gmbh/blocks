(ns ch.codesmith.blocks.aero-test
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.aero :as cba]
            [clojure.java.io :as io]
            [clojure.test :refer [deftest is]]))

(defn aero-test [aero-config expected-test]
  (let [base {:name   :aero-test
              :blocks {::aero {:type ::cba/aero}}}
        profile {:profile :test
                 :blocks  {::aero (assoc aero-config
                                    :profile :test)}}]
    (cb/with-system [system [base profile]]
      (is (= expected-test (cb/resolve-block system ::aero))))))

(deftest aero-config-correctness
  (aero-test {} {:b 2})
  (aero-test {:config {:file (io/resource "config2.edn")}}
             {:b 3}))

(defmacro config-throws [config]
  `(is (~'thrown? Exception
         (aero-test {:config ~config}
                    {:a 1}))))

(deftest aero-config-completeness
  (config-throws {:file (io/file "config3.edn")})
  (config-throws {:resource "config4.edn"}))
