(ns ch.codesmith.blocks.rabbitmq.dev
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.rabbitmq :as cbr]
            [ch.codesmith.blocks.test-containers.rabbitmq :as tcr]
            [clj-test-containers.core :as tc]
            [integrant.core :as ig]
            [langohr.core :as rmq]))

(defmethod ig/init-key ::test-container [_ _]
  (let [rabbitmq-container (-> (tcr/rabbitmq-container {})
                               (tc/start!))]
    {:container  rabbitmq-container
     :connection (rmq/connect (tcr/connection-config rabbitmq-container))}))

(defmethod ig/resolve-key ::test-container [_ instance]
  (:connection instance))

(defmethod ig/halt-key! ::test-container [_ {:keys [container connection]}]
  (rmq/close connection)
  (tc/stop! container))

(derive ::test-container cbr/connection)

(defn open-admin-web-ui
  [system block-key]
  (-> system
      (cb/get-block block-key)
      :instance
      :container
      tcr/open-admin-web-ui))
