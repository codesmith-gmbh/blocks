(ns ch.codesmith.blocks.rabbitmq.dev-test
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.rabbitmq.dev :as cbrd]
            [clojure.test :refer [deftest is testing]]
            [langohr.basic :as lb]
            [langohr.channel :as lch]
            [langohr.consumers :as lc]
            [langohr.queue :as lq])
  (:import (java.nio.charset StandardCharsets)))

(deftest rabbitmq-dev-correctness
  (testing "Testing the correctness of the RabbitMQ dev block.
test largely inspired by the Hello World example of the langohr library."
    (let [base {:name   :rabbitmq
                :blocks {::rabbitmq-dev {:type ::cbrd/test-container}}}
          response (promise)]
      (cb/with-system [system [base]]
        (let [connection (cb/resolve-block system ::rabbitmq-dev)
              qname "ch.codesmith.blocks.rabbitmq.dev-test"
              message "dev-test"]
          (with-open [ch (lch/open connection)]
            (lq/declare ch qname {:exclusive false :auto-delete true})
            (lc/subscribe ch qname (fn [_ _ ^bytes payload]
                                     (deliver response (String. payload StandardCharsets/UTF_8))))
            (lb/publish ch "" qname message {:content/type "text/plain"})
            (is (= message
                   (deref response 2000 nil)))))))))