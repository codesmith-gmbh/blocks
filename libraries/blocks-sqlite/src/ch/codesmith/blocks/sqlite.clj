(ns ch.codesmith.blocks.sqlite
  (:require [ch.codesmith.blocks :as cb]
            [ch.codesmith.blocks.jdbc :as cbjdbc]
            [clojure.spec.alpha :as s]
            [integrant.core :as ig]))

(derive ::sqlite-datasource cbjdbc/jdbc-datasource)

(def datasource-defaults
  {:datasource {:maximumPoolSize 1}})

(defmulti check-config ::type)

(s/def ::file string?)
(s/def ::file-config (s/keys :req-un [::file]))
(def file-config-checker (cb/checker ::file-config))

(defmethod check-config :file
  [config]
  (file-config-checker config))

(s/def ::name string?)
(s/def ::memory-config (s/keys :opt-un [::name]))
(def memory-config-checker (cb/checker ::memory-config))

(defmethod check-config :memory
  [config]
  (memory-config-checker config))

(defmulti jdbc-url ::type)

(defmethod jdbc-url :file
  [{:keys [file]}]
  (str "jdbc:sqlite:file:" file))

(defmethod jdbc-url :memory
  [{:keys [name] :or {name "memdb"}}]
  (str "jdbc:sqlite:file:" name "?mode=memory&cache=shared"))

(defmethod ig/init-key ::sqlite-datasource [_ config]
  (let [config (check-config (cb/deep-merge datasource-defaults config))
        config (assoc config :datasource {:jdbcUrl (jdbc-url config)})]
    (cbjdbc/init config)))

(defmethod ig/halt-key! ::sqlite-datasource [_ instance]
  (cbjdbc/halt! instance))
