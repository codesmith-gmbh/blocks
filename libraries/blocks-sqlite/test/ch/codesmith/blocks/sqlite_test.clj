(ns ch.codesmith.blocks.sqlite-test
  (:require
    [babashka.fs :as fs]
    [ch.codesmith.blocks :as cb]
    [ch.codesmith.blocks.jdbc.migratus :as migratus]
    [ch.codesmith.blocks.sqlite :as cbsqlite]
    [clojure.test :refer [deftest is]]
    [next.jdbc :as jdbc]))

(defn sqlite-correctness-test [config]
  (let [base {:name   :sqlite
              :blocks {::sqlite {:type   ::cbsqlite/sqlite-datasource
                                 :config {:migration-function migratus/migratus-migrate-function
                                          :migration-config   {:migration-dir "sqlite/migrations"}}}}}
        profile {:profile :memory
                 :blocks  {::sqlite {:config config}}}]
    (cb/with-system
      [system [base profile]]
      (is (= 1
             (let [connectable (cb/resolve-block system ::sqlite)]
               (:test (jdbc/execute-one! connectable ["SELECT TEST FROM TEST"]))))))))

(deftest sqlite-correctness
  (sqlite-correctness-test {::cbsqlite/type :memory})
  (let [dbfile (fs/create-temp-file {:suffix ".sqlite"})]
    (sqlite-correctness-test {::cbsqlite/type :file
                              :file           (str dbfile)})
    (is (fs/exists? dbfile))))
