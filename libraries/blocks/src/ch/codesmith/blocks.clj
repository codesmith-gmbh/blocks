(ns ch.codesmith.blocks
  (:refer-clojure :exclude [ref])
  (:require [clojure.set :as set]
            [clojure.spec.alpha :as s]
            [clojure.string :as str]
            [integrant.core :as ig]
            [weavejester.dependency :as dep]))

(s/def ::type keyword?)
(s/def ::config any?)
(s/def ::wrap-fn ifn?)
(s/def ::block-key (fn [value]
                     (or
                       (keyword? value)
                       (vector? value))))

(s/def ::block (s/keys :opt-un [::type ::config ::wrap-fn]))

(s/def ::name keyword?)
(s/def ::blocks (s/map-of ::block-key ::block))
(s/def ::base-config (s/keys :req-un [::name]
                       :opt-un [::blocks]))

(s/def ::profile keyword?)
(s/def ::profile-config (s/keys :req-un [::profile]
                          :opt-un [::blocks]))

(defonce
  ^{:doc      "Return a unique keyword that is derived from an ordered collection of named.
  The function will return the same keyword for the same collection.
  Cf. [[integrant.core/composite-keyword]]"
    :arglists '([nameds])}
  composite-keyword
  (memoize
    (fn [nameds]
      (let [parts  (mapv
                     (fn [named]
                       (let [name (name named)]
                         (if-let [ns (namespace named)]
                           (str ns "." name)
                           name)))
                     nameds)
            prefix (str (str/join "+" parts) "_")]
        (keyword "blocks.composite" (str (gensym prefix)))))))

(defn coerce-keyword [ref]
  (cond
    (vector? ref) (composite-keyword ref)
    (keyword? ref) ref
    :else (ex-info (str "Invalid reference " ref) {:ref ref})))

(defn ref [k]
  (ig/ref (coerce-keyword k)))

(defrecord SymbolicRef [ref])

(defn symbolic-ref
  "Creates a Symbolic Ref. It is simply a marker for a ref with symbols that can
  be used to walk a config map when using symbols as a templating mechanism."
  [ref]
  (->SymbolicRef ref))

(defn checker [spec]
  #(if (s/valid? spec %)
     %
     (throw (ex-info (str "The value " % " is not conform to its spec")
              {:schema      spec
               :value       %
               :explanation (s/explain-data spec %)}))))

(def check-base-config (checker ::base-config))
(def check-profile-config (checker ::profile-config))

;; some utils

(defmethod ig/init-key ::identity [_ value]
  value)

(defn envvar [{:keys [envvar coerce]}]
  (let [value (System/getenv envvar)]
    (if coerce
      (coerce value)
      value)))

(defmethod ig/init-key ::envvar [_ value]
  (envvar value))

(defn slurp-file [{:keys [file slurp] :or {slurp clojure.core/slurp}}]
  (slurp file))

(defmethod ig/init-key ::file [_ value]
  (slurp-file value))

;; system

;; Copied verbatim from the defunct clojure-contrib (http://bit.ly/deep-merge-with)
(defn deep-merge-with [f & maps]
  (apply
    (fn m [& maps]
      (if (every? map? maps)
        (apply merge-with m maps)
        (apply f maps)))
    maps))

(defn deep-merge [& maps]
  (apply deep-merge-with (fn [_ val] val) maps))

;; Init / Halt! / Ignition

(defn blocks [{:keys [blocks]}]
  (or blocks {}))

(defn components [{:keys [components]}]
  (or components {}))

(defn merge-with-profiles [base & profiles]
  (check-base-config base)
  (doseq [profile profiles]
    (check-profile-config profile))
  {:name       (:name base)
   :profiles   (mapv :profile profiles)
   :blocks     (apply deep-merge (blocks base) (map blocks profiles))
   :components (apply deep-merge (components base) (map components profiles))})

(defn halt! [{:keys [system]}]
  (when system (ig/halt! system)))

(defn component-key [components-keys key]
  (get components-keys key key))

(defn compute-component-keys [blocks]
  (into {}
    (keep (fn [[key {:keys [type]}]]
            (when (nil? type)
              (throw (ex-info (str "The key " key " has no type")
                       {:key key})))
            [key [type (if (vector? key)
                         (composite-keyword key)
                         key)]]))
    blocks))

(defn raw-ig-config [derivations blocks components]
  (into (or components {})
    (map (fn [[key {:keys [config]}]]
           [(derivations key) config]))
    blocks))

;; copied from integrant to use a cache of the dependency graph
;; as integrant builds it on every "build" call (bad performance for ignition).

(defn find-keys [config no-refsets-graph key-comparator keys f]
  (let [keyset       (set (mapcat #(map key (ig/find-derived config %)) keys))
        dependencies (f no-refsets-graph keyset)]
    (->> dependencies
      (set/union keyset)
      (sort key-comparator))))

(defn dependent-keys [config no-refsets-graph key-comparator keys]
  (find-keys config no-refsets-graph key-comparator keys dep/transitive-dependencies-set))

(defn- build′ [config no-refsets-graph key-comparator keys f]
  (let [relevant-keys   (dependent-keys config no-refsets-graph key-comparator keys)
        relevant-config (select-keys config relevant-keys)]
    (when-let [invalid-key (first (#'ig/invalid-composite-keys config))]
      (throw (#'ig/invalid-composite-key-exception config invalid-key)))
    (when-let [ref (first (#'ig/ambiguous-refs relevant-config))]
      (throw (#'ig/ambiguous-key-exception config ref
               (map key (#'ig/find-derived config ref)))))
    (when-let [refs (seq (#'ig/missing-refs relevant-config))]
      (throw (#'ig/missing-refs-exception config refs)))
    (reduce (partial #'ig/build-key f #'ig/wrapped-assert-key ig/resolve-key)
      (with-meta {} {::ig/origin config})
      (map (fn [k] [k (config k)]) relevant-keys))))

(defn init″ [config no-refsets-graph key-comparator keys]
  (build′ config no-refsets-graph key-comparator keys ig/init-key))

(defn- init′
  ([{:keys [blocks components] :as config}]
   (init′ config (set (concat
                        (keys blocks)
                        (keys components)))))
  ([{:keys [name profiles blocks components]} keys]
   (let [component-keys   (compute-component-keys blocks)
         ig-config        (raw-ig-config component-keys blocks components)
         ig-config        (ig/expand ig-config)
         no-refsets-graph (ig/dependency-graph ig-config {:include-refsets? false})
         key-comparator   (ig/key-comparator (ig/dependency-graph ig-config))
         keys             (mapv (partial component-key component-keys) keys)]
     (try
       {:system           (init″ ig-config no-refsets-graph key-comparator keys)
        :ig-config        ig-config
        :no-refsets-graph no-refsets-graph
        :key-comparator   key-comparator
        :name             name
        :profiles         profiles
        :derivations      component-keys}
       (catch Exception e
         (halt! {:system (:system (ex-data e))})
         (throw e))))))

(defn init
  ([base+profiles]
   (init′ (apply merge-with-profiles base+profiles)))
  ([base+profiles keys]
   (init′ (apply merge-with-profiles base+profiles) keys)))

(defn ignite!
  ([base+profiles]
   (init base+profiles []))
  ([{:keys [system ig-config derivations no-refsets-graph key-comparator] :as state} additional-keys]
   (assoc state :system
                (build′
                  ig-config
                  no-refsets-graph key-comparator
                  (into (set (keys system))
                    (map (partial component-key derivations))
                    additional-keys)
                  (fn [k v]
                    (if (contains? system k)
                      (system k)
                      (ig/init-key k v)))))))

(defn quench! [{:keys [system ig-config no-refsets-graph key-comparator] :as state} keys]
  (let [[halted-keys left-keys]
        (reduce
          (fn [[halted-keys left-keys] key]
            (when-some [value (system key)]
              (ig/halt-key! key value))
            [(conj halted-keys key) (disj left-keys key)])
          [#{} (set (clojure.core/keys system))]
          (#'ig/reverse-dependent-keys
            (#'ig/system-origin system)
            keys))]
    (vary-meta
      (assoc state :system
                   (build′ ig-config
                     no-refsets-graph key-comparator
                     left-keys
                     (fn [k _]
                       (system k))))
      assoc
      ::halted-keys halted-keys)))

(defn re-ignite! [state keys &
                  {:keys [with-descendents?]
                   :or   {with-descendents? true}}]
  (let [state (quench! state keys)]
    (ignite! state
      (if with-descendents?
        (-> state meta ::halted-keys)
        keys))))

(defn get-block [{:keys [system derivations]} key]
  (get system (component-key derivations key)))

(defn resolve-block [{:keys [derivations] :as instance} key]
  (ig/resolve-key (component-key derivations key) (get-block instance key)))

;; with system-instance

(defmacro with-system
  [[var base+profiles] & body]
  `(let [~var (init ~base+profiles)]
     (try
       ~@body
       (finally
         (halt! ~var)))))
