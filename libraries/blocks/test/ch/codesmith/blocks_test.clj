(ns ch.codesmith.blocks-test
  (:require [ch.codesmith.blocks :as cb]
            [clojure.java.io :as io]
            [clojure.test :refer [deftest is]]
            [clojure.tools.reader.edn :as edn]
            [integrant.core :as ig]))

(deftest envvar-block-correctness
  (let [base {:name   :envvar-block
              :blocks {::envvar {:type   ::cb/envvar
                                 :config {:envvar "HOME"
                                          :coerce io/file}}}}]
    (cb/with-system [system [base]]
      (is (= (io/file (System/getProperty "user.home"))
            (cb/resolve-block system ::envvar))))))

(deftest identity-block-correctness
  (let [base {:name   :identity-block
              :blocks {::id {:type   ::cb/identity
                             :config 1}}}]
    (cb/with-system [system [base]]
      (is (= 1 (cb/resolve-block system ::id))))))

(deftest file-block-correctness-slurp
  (let [base {:name   :file-block
              :blocks {::file
                       {:type   ::cb/file
                        :config {:file (io/resource "config.edn")}}}}]
    (cb/with-system [system [base]]
      (is (= "{:b 2}" (cb/resolve-block system ::file))))))

(deftest file-block-correctness-edn
  (let [base {:name   :file-block
              :blocks {::file
                       {:type   ::cb/file
                        :config {:file  (io/resource "config.edn")
                                 :slurp (fn [file]
                                          (edn/read-string (slurp file)))}}}}]
    (cb/with-system [system [base]]
      (is (= {:b 2} (cb/resolve-block system ::file))))))

(deftest profiles-correctness
  (let [base     {:name   :profiles-test
                  :blocks {::test1 {:type ::cb/identity}
                           ::test2 {:type ::cb/identity}}}
        profile1 {:profile :first
                  :blocks  {::test1 {:config "1"}
                            ::test2 {:config {:a 1}}}}
        profile2 {:profile :second
                  :blocks  {::test1 {:config "2"}
                            ::test2 {:config {:b 2}}}}]
    (cb/with-system [system [base profile1 profile2]]
      (is (= "2" (cb/resolve-block system ::test1)))
      (is (= {:a 1 :b 2} (cb/resolve-block system ::test2))))))

(deftest components-correctness
  (let [base {:name       :components-correctness
              :blocks     {::a {:type   ::cb/identity
                                :config 1}}
              :components {[::cb/identity ::b] 2}}]
    (cb/with-system [system [base]]
      (is (= 1 (cb/resolve-block system ::a)))
      (is (= 2 (cb/resolve-block system [::cb/identity ::b]))))))

(deftest re-ignite!-correctness
  (let [base  {:name       :components-correctness
               :blocks     {::a {:type   ::cb/identity
                                 :config (ig/ref [::cb/identity ::b])}}
               :components {[::cb/identity ::b] 2}}
        check (fn [system a b]
                (is (= a (cb/resolve-block system ::a)))
                (is (= b (cb/resolve-block system [::cb/identity ::b]))))]
    ;; we first assert that the base is correct
    (cb/with-system [system [base]]
      (check system 2 2))
    (let [system (cb/ignite! [base])]
      (check system nil nil)
      (let [system (cb/ignite! system [[::cb/identity ::b]])]
        (check system nil 2)
        (let [system (cb/ignite! system [::a])]
          (check system 2 2)
          (let [system (cb/re-ignite! system [[::cb/identity ::b]])]
            (check system 2 2)
            (let [system (cb/re-ignite! system [[::cb/identity ::b]]
                           :with-descendents? false)]
              (check system nil 2))))))
    (let [system (cb/ignite! [base])]
      (check (cb/ignite! system [::a]) 2 2))))