# CHANGELOG

## Unreleased

## 0.11.156 (2024-11-27)

### Added

- `:init-sql-statements` options for the embedded postgres db.

## 0.11.155 (2024-09-27)

### Added

- schema options for the liquibase migration function

## 0.11.150 (2024-09-11)

### Changed

- blocks-dev: dev agent (for ignition on repl) with error handler
- bump dependencies.

## 0.11.146 (2024-09-01)

### Changed

- copied some internal integrant functions to cache the dependency graph and key comparator.

## 0.11.143 (2024-09-01)

### Changed

- BREAKING CHANGE: internal blocks api to compute component keys from block keys.

## 0.11.140 (2024-09-01)

### Removed

- BREAKING CHANGE: remove block transformation.

### Changed

- allow block names to be vectors of `named` values in addition to vectors.
- bump dependencies.

### Fixed

- ig-config is properly expanded in a system.

## 0.11.136 (2024-07-25)

### Changed

- integrant 0.10.0: BREAKING: we do not use `prep` anymore, but the new `expand` instead.
- bump other dependencies

## 0.10.133 (2024-02-14)

### Changed

- update dependencies
- suppress CVE-2017-20189 (we use Clojure 1.11.1)

## 0.10.128 (2023-11-18)

### Added

- export clj-kondo linting config.

### Changed

- bump dependencies

## 0.10.124 (2023-11-04)

### Added

- add `force-stop?` option to embedded postgres.
- `cb/ignite!`, `cb/quench!`, `cb/re-ignite` for dynamic systems.
- xtdb: assoc in the ctx a function to get any db value

### Fixed

- allow `stop-embedded-postgres` to always succeed.
- fix namespace for `ch.codesmith.blocks.xtdb.ring` (BREAKING CHANGE)
- derive `:ch.codesmith.blocks.reitit.dev/dynamic-router` as router.
- fix `cb/resolve-block` to return nil instead of the key if the key is not present in the system.

### Changed

- bumped dependencies

## 0.9.111 (2023-06-25)

### Fixed

- `get-block` and `resolve-block` in `ch.codesmith.blocks.dev`.

## 0.9.108 (2023-06-25)

### Added

- support for `next.jdbc/with-options` when creating a DataSource
- optional next.jdbc.protocol/Sourceable derivation for keywords for jdbc dev.
- support for integrant components on bases and profiles
- support for [sqitch](https://sqitch.org) as migration method; for tag checking in jdbc and actual migration in dev.

### Changes

- no manual derivations internally.

## 0.9.96 (2023-06-11)

### Changes

- start the system partially (keys argument in blocks/init and dev/go)

### Fixed

- init and dev/go

## 0.9.92 (2023-06-11)

### FULL BREAKING CHANGE

new ideas with profile merging, auto-derive; blocks are now normal integrant components.

### Added

- more data in the default exception handler.

## 0.8.85 (2023-05-20)

### Changed

- bumped dependencies

## 0.8.82 (2023-04-07)

### Added

- blocks for sqlite.
- a liquibase migration function. liquibase is optional

### Changed

- breaking change: migrations are optional. migratus is optional.

## 0.7.77 (2022-12-10)

### Changed

- breaking change: use fully namespaced keywords for blocks stuff
- breaking change: submap for datasource properties.
- breaking change: move some postres-dev functions to jdbc-dev

- postgres: :migratus key renamed to :migration-config
- postgres: :connection-url renamed to :jdbcUrl

### Added

- postgres/postgres-dev: allow to pass an arbitrary migration function (and config).

- jdbc block for common jdbc functionality

- bump dependencies

## 0.5.58 (2022-06-12)

### Fixed

- allows dynamic router with a non var value, albeit with a force flag to make the user understand
  the caveats: the function must have a var resolution of a route function inside, so that function
  redefinition really is used to compute the routes on the next call.

## 0.5.54 (2022-06-12)

### Added

- value basic block to switch between constants, envvars and file content between
  environments.

## 0.5.50 (2022-06-12)

### Changed

- dependency updates

## 0.5.46 (2022-05-29)

### Fixed

- The halt function for many components

### Added

- `cb/resolve-block` to resolve a block from a system with the block-key; usefull for tests and repl
- block-dev project for development (delegates most to integrant-repl)

### Changed

- dependency updates, in particult xtdb in version 1.21.0

## 0.5.34 (2022-05-07)

**Breaking Change**: the construction is different.

### Fixed

- fix blocks-rabbitmq dependency for blocks-rabbitmq-dev

## 0.4.14 (2022-04-21)

### Added

- blocks for rabbitmq

## 0.4.10 (2022-04-18)

### Changes

- new repository with all libraries together.

### Fixes

- aero config with file will now fails if file does not exists.

## 0.3.28

### Changed

- Tooling

## 0.3.22

### Changed

- new anvil tooling version

## 0.3.20

### Changed

- using anvil tooling for building and deployment

## 0.2.12 (2022-01-02 / 4b31c52)

### Added

- block transform multimethods for [integrant](https://github.com/weavejester/integrant).
- config blocks with [aero](https://github.com/juxt/aero).
- awesome [tooling](https://github.com/lambdaisland/open-source) by [lambdaisland](https://lambdaisland.com).